﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeaRiskAnalysis.Model
{
    public class ChoiceModel
    {
        public string description { get; set;}
        public int minRiskPercentage { get; set; }
        public int maxRiskPercentage { get; set; }
        public int singleRiskValue { get; set; }
    }
}
