﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeaRiskAnalysis.Model
{
    public class QuestionModel
    {
        public double weight { get; set; }
        public string description { get; set; }
        public List<ChoiceModel> choices { get; set; }
        public int beginLine { get; set; }
        public int endLine { get; set; }

        public QuestionModel()
        {
            choices = new List<ChoiceModel>();
        }
    }
}
