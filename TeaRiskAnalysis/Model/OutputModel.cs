﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeaRiskAnalysis.Model
{
    public class OutputModel
    {
        public int endLine { get; set; }
        public double riskWeighted { get; set; }
    }
}
