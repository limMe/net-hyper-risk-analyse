﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeaRiskAnalysis.Model
{
    public class ResultModel
    {
        public int aspect { get; set; }
        public int beginLine { get; set; }
        public int endLine { get; set; }
        public int question { get; set; }
        public int risk { get; set; }
        public double aspectWeight { get; set; }
        public double weight { get; set; }
        public string qDescribe { get; set; }
        public string aDescribe { get; set; }
    }
}
