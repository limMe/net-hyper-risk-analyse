﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeaRiskAnalysis.Model
{
    class StructedQuestion
    {
        public double riskWeighted { get; set; }
        public double percentage { get; set;}
        public string describe { get; set; }
    }

    class StructedAspect
    {
        public int aspectNum { get; set; }
        public double weight { get; set; }
        public double riskWeighted { get; set; }
        public double percentage { get; set; }
        public string describe { get; set; }
        public List<StructedQuestion> questions { get; set; }
        public StructedAspect() {
            questions = new List<StructedQuestion>();
        }
    }

    class StructedOutputModel
    {
        public double totalRisk { get; set; }
        public List<StructedAspect> aspects { get; set; }
        public StructedOutputModel()
        {
            aspects = new List<StructedAspect>();
        }
    }
}
