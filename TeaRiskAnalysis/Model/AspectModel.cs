﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeaRiskAnalysis.Model
{
    public class AspectModel
    {
        public double weight { get; set; }
        public string description { get; set; }
        public int beginLine { get; set; }
        public int endLine { get; set; }
        public List<QuestionModel> questions { get; set; }

        public AspectModel()
        {
            questions = new List<QuestionModel>();
        }
    }
}
