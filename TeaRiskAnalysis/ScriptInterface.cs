﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Newtonsoft.Json;
using TeaRiskAnalysis.Model;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace TeaRiskAnalysis
{
    //Need this Attribute
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class ScriptInterface
    {
        private List<OutputModel> outputs = new List<OutputModel>();
        List<double> tempRisks = new List<double>();

        //TODO: Same to Form1.cs, consider one static class to maintain this.
        private static string fileName = "Original.xlsx";
        private static string sheetName = "Sheet1";

        private static string rateRow = "G";//One Radio Choice
        private static string riskRow = "F";//One Radio Choice
        private static string aspectRow = "A";
        private static string questionRow = "C";
        private static string choiceRow = "E";
        private static string questionWeightRow = "D";
        private static string aspectWeightRow = "B";//New
        private static string questionRiskRow = "G";
        private static string aspectRiskRow = "H";
        private static string totalRiskRow = "I";

        public Form1 formDelegate = null;

        //Need to be public
        /// <summary>
        /// 测试方法
        /// </summary>
        /// <param name="ha"></param>
        public void callMe(string ha)
        {
            MessageBox.Show(ha);
        }   

        /// <summary>
        /// JS读完算完，回调这个方法
        /// </summary>
        /// <param name="data"></param>
        public string callBack(string data)
        {
            var datalist = JsonConvert.DeserializeObject<List<ResultModel>>(data);
            if(this.formDelegate != null)
            {
                this.formDelegate.hideAllDecoration();
            }
            writeExcel(datalist);

            //再次还给JS
            //我不管了，乱就乱
            var resultToJs = new StructedOutputModel();
            resultToJs.totalRisk = datalist.Sum(x => x.aspectWeight * x.weight * x.risk);
            StructedAspect aAspect = null;
            for(int i = 0; i < datalist.Count; i++)
            {
                var aQuestion = new StructedQuestion();
                aQuestion.describe = datalist[i].qDescribe;
                aQuestion.riskWeighted = datalist[i].risk * datalist[i].weight;
                aQuestion.percentage = aQuestion.riskWeighted / resultToJs.totalRisk * 100;
                if(aAspect == null || aAspect.aspectNum != datalist[i].aspect)
                {
                    aAspect = new StructedAspect();
                    resultToJs.aspects.Add(aAspect);
                    aAspect.aspectNum = datalist[i].aspect;
                    aAspect.describe = datalist[i].aDescribe;
                    aAspect.weight = datalist[i].aspectWeight;
                }
                aAspect.questions.Add(aQuestion);
            }
            foreach(StructedAspect aspect in resultToJs.aspects)
            {
                aspect.riskWeighted = aspect.questions.Sum(x => x.riskWeighted * aspect.weight);
                aspect.percentage = aspect.riskWeighted / resultToJs.totalRisk * 100;
            }
            Clipboard.SetText(JsonConvert.SerializeObject(resultToJs));
            return JsonConvert.SerializeObject(resultToJs);
        }

        private void writeExcel(List<ResultModel> datalist)
        {
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, true))
            {
                // Retrieve a reference to the workbook part.
                WorkbookPart wbPart = document.WorkbookPart;

                // Find the sheet with the supplied name, and then use that 
                // Sheet object to retrieve a reference to the first worksheet.
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                  Where(s => s.Name == sheetName).FirstOrDefault();

                // Throw an exception if there is no sheet.
                if (theSheet == null)
                {
                    throw new ArgumentException("sheetName");
                }

                // Retrieve a reference to the worksheet part.
                WorksheetPart wsPart =
                    (WorksheetPart)(wbPart.GetPartById(theSheet.Id));


                //问题的风险第一项
                var theCell = OpenXMLHelper.InsertCellInWorksheet(questionRiskRow, 4, wsPart);
                theCell.CellValue = new CellValue((datalist[0].weight*datalist[0].risk).ToString());
                theCell.DataType = new EnumValue<CellValues>(CellValues.Number);

                //问题风险后几个
                for (int i = 1; i < datalist.Count; i++)
                {
                    var aCell = OpenXMLHelper.InsertCellInWorksheet(questionRiskRow, (UInt32)(datalist[i-1].endLine + 1), wsPart);
                    aCell.CellValue = new CellValue((datalist[i].weight * datalist[i].risk).ToString());
                    aCell.DataType = new EnumValue<CellValues>(CellValues.Number);
                }

                var aspectMax = datalist.Max(x => x.aspect);
                //要求必须有序
                for(int currentAspect = 0; currentAspect <= aspectMax; currentAspect++)
                {
                    var riskByAspect = datalist.Where(x => x.aspect == currentAspect)
                        .Sum(x => x.risk * x.weight);
                    double riskByAspectWeighted = riskByAspect * datalist.Where(x => x.aspect == currentAspect)
                        .First().aspectWeight;
                    var riskByAspectLine = datalist.Where(x => x.aspect == currentAspect)
                        .Min(x => x.beginLine);
                    var aCell = OpenXMLHelper.InsertCellInWorksheet(aspectRiskRow, (UInt32)riskByAspectLine, wsPart);
                    aCell.CellValue = new CellValue(riskByAspectWeighted.ToString());
                    aCell.DataType = new EnumValue<CellValues>(CellValues.Number);
                }

                var totalRisk = datalist.Sum(x => x.aspectWeight * x.weight * x.risk);
                var totalRiskLine = datalist.Min(x => x.beginLine);
                var finalCell = OpenXMLHelper.InsertCellInWorksheet(totalRiskRow, (UInt32)totalRiskLine, wsPart);
                finalCell.CellValue = new CellValue(totalRisk.ToString());
                finalCell.DataType = new EnumValue<CellValues>(CellValues.Number);

                /*
                double total = 0.0;
                foreach(OutputModel output in outputs)
                {
                    total = total + output.riskWeighted;
                }

                //总风险
                var totalCell = OpenXMLHelper.InsertCellInWorksheet("I", 3, wsPart);
                totalCell.CellValue = new CellValue((total * 0.01).ToString());
                totalCell.DataType = new EnumValue<CellValues>(CellValues.Number);
                */

                wsPart.Worksheet.Save();
            }
        }

        /// <summary>
        /// Old
        /// 读完一个Aspect后计算
        /// </summary>
        /// <param name="datalist"></param>
        /// <param name="i"></param>
        private void calcAspect(List<ResultModel> datalist, int i)
        {
            //读完了整个aspect开始算风险
            if (tempRisks.Count > 0)
            {
                var newOutput = new OutputModel();
                if (tempRisks.Count == 2)
                {
                    var max = Math.Max(tempRisks[0], tempRisks[1]);
                    newOutput.riskWeighted = datalist[i - 1].weight * max;
                }
                else
                {
                    double total = 0.0;
                    foreach (double d in tempRisks)
                    {
                        total = total + d;
                    }
                    double average = total / tempRisks.Count;
                    newOutput.riskWeighted = datalist[i - 1].weight * average;
                }
                newOutput.endLine = datalist[i - 1].endLine;
                outputs.Add(newOutput);
                tempRisks.Clear();
            }
        }

    }
}
