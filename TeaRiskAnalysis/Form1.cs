﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TeaRiskAnalysis.Model;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;

namespace TeaRiskAnalysis
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void hideAllDecoration() {
            this.pictureBox1.Visible = false;
            this.pictureBox2.Visible = false;
        }

        //Please, do not change the filename and sheetname
        private static string fileName = "Original.xlsx";
        private static string sheetName = "Sheet1";

        /// <summary>
        /// Model层第一层级是AspectModel
        /// AspectModel里会包含一个List<QuestionModel>
        /// QuestionModel里会包含一个List<ChoiceModel>
        /// </summary>
        private List<AspectModel> result = new List<AspectModel>();

        /// <summary>
        /// 初始数据的行数
        /// </summary>
        private int currentLine = 4;

        /// <summary>
        /// Step 1. 使用openExcel()打开读取Excel文件，并且把数据输出为一个js文件，同步阻塞。
        /// Step 2. 把ScriptInterface添加到WebBrowser，并且加载index.html
        /// Step 3. 读完的事情交给index.html的JavaScript处理，最后回掉
        /// Step 4. 相当于这里只处理开始的预备工作，JS处理交互期间的，ScriptInterface专门擦屁股
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            //CreateSpreadsheetWorkbook("C:\\Users\\zhongdian\\Desktop\\Test.xlsx");
            openExcel();
            string currentDir = Directory.GetCurrentDirectory();
            webBrowser.Url = new Uri(String.Format("file:///{0}/index.html", currentDir));
            var scriptListener = new ScriptInterface();
            scriptListener.formDelegate = this;
            webBrowser.ObjectForScripting = scriptListener;
            webBrowser.ScriptErrorsSuppressed = false;
        }

        private static string rateRow = "G";//One Radio Choice
        private static string riskRow = "F";//One Radio Choice
        private static string aspectRow = "A";
        private static string questionRow = "C";
        private static string choiceRow = "E";
        private static string questionWeightRow = "D";
        private static string aspectWeightRow = "B";//New
        private void openExcel()
        {

            using(SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, false))
            {
                //WorkbookPart是整个文档的根基
                WorkbookPart wbPart = document.WorkbookPart;

                //Find the sheet with name, using Lambda
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                    Where(s => s.Name == sheetName).FirstOrDefault();
                
                //No sheet?
                if(theSheet == null)
                {
                    MessageBox.Show("读取文件错误，确保当前目录下拥有Original.xlsx文件，并且拥有“总体指标体系”工作表，且没有被其他应用打开。");
                    return;
                }

                //又是一个层级
                WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                //检测数据有没有结束
                while (OpenXMLHelper.getCellValue(wbPart, wsPart, riskRow + currentLine.ToString()) != "")
                {
                    //如果当前单元格有顶级指标内容，则新建一个Aspect
                    if (OpenXMLHelper.getCellValue(wbPart, wsPart, aspectRow + currentLine.ToString()) != "")
                    {
                        var newAspect = new AspectModel();
                        newAspect.beginLine = currentLine;
                        newAspect.description = OpenXMLHelper.getCellValue(wbPart, wsPart, aspectRow + currentLine.ToString());
                        //New: 有时候，aspect也需要加权
                        double weight = 0.0;
                        Double.TryParse(OpenXMLHelper.getCellValue(wbPart, wsPart, aspectWeightRow + currentLine.ToString()), out weight);
                        newAspect.weight = weight;

                        //记录上一个三级指标的结束行
                        if (result.Count != 0)
                        {
                            result[result.Count - 1].endLine = currentLine - 1;
                            var questionCount = result[result.Count - 1].questions.Count;
                            result[result.Count - 1].questions[questionCount - 1].endLine = currentLine - 1;

                        }

                        result.Add(newAspect);
                    }
                    processLine(wbPart, wsPart);
                    currentLine++;
                }

                FileStream fs = new FileStream("data.js", FileMode.Create);
                StreamWriter sw = new StreamWriter(fs,Encoding.UTF8);
                sw.Write("var data =" + JsonConvert.SerializeObject(result));
                sw.Close();
                fs.Close();
                //MessageBox.Show(JsonConvert.SerializeObject(result));
            }
        }

        /// <summary>
        /// 本质上是把目前行作为Choice添加到当前最后一个Aspect的最后一个Question
        /// 如果这是Question的开始行，则先添加一个Question到Aspect
        /// </summary>
        /// <param name="wbPart"></param>
        /// <param name="wsPart"></param>
        private void processLine(WorkbookPart wbPart, WorksheetPart wsPart)
        {
            if(OpenXMLHelper.getCellValue(wbPart,wsPart,questionRow+currentLine.ToString()) != "")
            {
                var newQuestion = new QuestionModel();
                newQuestion.beginLine = currentLine;
                newQuestion.description = OpenXMLHelper.getCellValue(wbPart, wsPart, questionRow + currentLine.ToString());
                double weight = 0.0;
                //如果weight出来是0.0，那证明输出有错
                Double.TryParse(OpenXMLHelper.getCellValue(wbPart, wsPart, questionWeightRow + currentLine.ToString()), out weight);
                newQuestion.weight = weight;

                var nowCount = result[result.Count - 1].questions.Count;
                if (nowCount != 0) {
                    result[result.Count - 1].questions[nowCount - 1].endLine = currentLine - 1;
                }

                result[result.Count - 1].questions.Add(newQuestion);
            }
            var description = OpenXMLHelper.getCellValue(wbPart, wsPart, choiceRow + currentLine.ToString());
            var newChoice = new ChoiceModel();
            newChoice.description = OpenXMLHelper.getCellValue(wbPart, wsPart, choiceRow + currentLine.ToString());

            //Old Version
            /*
            var rawRisk = OpenXMLHelper.getCellValue(wbPart, wsPart, rateRow + currentLine.ToString());
            var rawRisks = rawRisk.Split('-');
            newChoice.minRiskPercentage = Int32.Parse(rawRisks[0]);
            newChoice.maxRiskPercentage = Int32.Parse(rawRisks[1].Split('%')[0]);
            */

            //New Version 
            var singleRiskValue = OpenXMLHelper.getCellValue(wbPart, wsPart, riskRow + currentLine.ToString());
            newChoice.singleRiskValue = Int32.Parse(singleRiskValue);

            var nowQuestion = result[result.Count - 1].questions.Count - 1;
            result[result.Count - 1].questions[nowQuestion].choices.Add(newChoice);
        }

    }
}
